/////////////////////////////////
// File: pioneer_flocking.cc
// Desc: Flocking behaviour, Stage controller demo
// Created: 2009.7.8
// Author: Richard Vaughan <vaughan@sfu.ca>
// License: GPL
/////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stage.hh"
using namespace Stg;

#define PI 3.1415926535


class Robot
{
public:
    Robot(ModelPosition *_pos)
    : pos(_pos)
    {
        // Do everything through the position callback
        pos->AddCallback(Model::CB_UPDATE, (model_callback_t)UpdateCallback, this);
        pos->Subscribe();

        // Get range sensors
        sonar           = (ModelRanger*)pos->GetChild("ranger:0");
        laser           = (ModelRanger*)pos->GetChild("ranger:1");
        sonar->Subscribe();
        laser->Subscribe();
        ccount          = 0;
        xvel            = 0;
        avel            = 0;
        id              = pos->GetId();
    }
    
    ModelPosition   *pos;
    ModelRanger     *laser;
    ModelRanger     *sonar;
    int             ccount;
    double          xvel,avel;
    uint32_t        id;
    
    static int UpdateCallback(Model *mod, Robot* robot)
    {
		return robot->Update();
    }
    
    int Update(void)
    {
        ccount++;
        calc_new_velocity();
        return 0;
    }
    
    void calc_new_velocity()
    {
        // Get the vector sum of all the range results
        std::vector<ModelRanger::Sensor> s = sonar->GetSensors();
        std::vector<ModelRanger::Sensor> l = laser->GetSensors();
     
        
        double x = 0;
        double y = 0;
        double front = 10000000;
        
        for(unsigned i=0; i<l.size(); i++)
        {
            double strength = (l[i].fov / l[i].sample_count) / (2*PI);
            for(unsigned j=0; j<l[i].ranges.size(); j++)
            {
                //printf("laser %d %d %f %f\n",i,j,l[i].ranges[j],l[i].bearings[j]+l[i].pose.a);
                double r = l[i].ranges[j];
                double b = l[i].bearings[j] + l[i].pose.a;
                x += (cos(b) * r + l[i].pose.x) * strength;
                y += (sin(b) * r + l[i].pose.y) * strength;
                if (fabs(b) < PI/2 && r < front)
                    front = r;
            }
        }
        for(unsigned i=0; i<s.size(); i++)
        {
            double strength = (s[i].fov / s[i].sample_count) / (2*PI);
            for(unsigned j=0; j<s[i].ranges.size(); j++)
            {
                //printf("sonar %d %d %f %f\n",i,j,s[i].ranges[j],s[i].bearings[j]+s[i].pose.a);
                double r = s[i].ranges[j];
                double b = s[i].bearings[j] + s[i].pose.a;
                x += (cos(b) * r + s[i].pose.x) * strength;
                y += (sin(b) * r + s[i].pose.y) * strength;
            }
        }
        //printf("Vector sum %f %f\n",x,y);
        
        double a = atan2(y, x);
        
        const double correctheading = PI/20;
        const double freespace      = 0.5;
        const double turngain       = 1.0;
        const double forwardspeed   = 0.4;

        //printf("id:%x ex:%f ey:%f vx:%f vy:%f va:%f front:%f\n",id, pos->est_pose.x, pos->est_pose.y, x, y, a, front);
        // Now do the adjustment of speed
        if (fabs(a) < correctheading && front > freespace)
        {
            pos->SetSpeed(forwardspeed, 0 , 0);
        }
        else{
            pos->SetSpeed(0, 0 , turngain * a);
        }
        

    }

};


// Stage calls this when the model starts up
extern "C" int Init( ModelPosition* mod )
{
    new Robot(mod);
    return 0;
}



