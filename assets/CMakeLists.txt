
FILE( GLOB pngs "*.png" )
FILE( GLOB opencl "*.cl" )

INSTALL(FILES ${pngs} rgb.txt ${opencl}
        DESTINATION share/stage/assets
)

INSTALL(FILES rgb.txt 
        DESTINATION share/stage
)

