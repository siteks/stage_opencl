#define STRINGIFY(x) #x

const char *raytracekernel = 
STRINGIFY(


//--------------- All this should be brought in from the main program
// perhaps autogenerate the source and prepend to the asset code
//__constant int ppm = 50;

__constant int RBITS = 5; // regions contain (2^RBITS)^2 pixels
__constant int SBITS = 5;// superregions contain (2^SBITS)^2 regions
//__constant int SRBITS = RBITS+SBITS;
__constant int SRBITS = 5+5;

__constant int REGIONWIDTH = 1<<5;
__constant int REGIONSIZE = (1<<5)*(1<<5);

__constant int SUPERREGIONWIDTH = 1<<5;
__constant int SUPERREGIONSIZE = (1<<5)*(1<<5);

__constant int CELLMASK = ~((~0x00)<< 5 );
__constant int REGIONMASK = ~((~0x00)<< 10 );

int GETCELL( const int x ) { return( x & CELLMASK); }
int GETREG(  const int x ) { return( ( x & REGIONMASK ) >> RBITS); }
int GETSREG( const int x ) { return( x >> SRBITS); }


typedef struct {
  int x;
  int y;
} Ipoint;

typedef struct {
  float   x;
  float   y;
  float   a;
} Pose;

typedef struct {
  Pose    origin;
  float   range;
} Ray;

// Type to hold matches
__constant int MPSIZE = 16;

typedef struct {
  int     hits;
  int     n;
  int     iter;
  float   x;
  float   y;
  float   a;
  Ipoint  p[16];
} Mpoint;



int GetSuperRegion(__global char *world, int x, int y)
{
  int srsize = *(__global int*)world;
  __global Ipoint *op = (__global Ipoint*)(world + 4);
  int srp = 4 + srsize * 8;

  for(int i = 0; i < srsize; i++)
  {
    if (op[i].x == x && op[i].y == y)
    {
      return srp + i * SUPERREGIONSIZE * 4;
    }
  }
  return 0;
}


__global char *GetRegion(__global char *world, int sr, int x, int y)
{
  // Add the x and y positions to the offset to the start of the 
  // superregions region 'pointers' (actually int offsets) in order to obtain
  // the offset into the regions cells. Turn this offset into a real pointer
  int cell_area_offset = *(__global int *)(world + sr + (x + y * SUPERREGIONWIDTH) * 4);
  return cell_area_offset ? (__global char *)world + cell_area_offset : 0;
}


__kernel void raytrace(
                    __global char     *world,   // Describes the world, and which cells are occupied
                    __global Ray      *rays,    // The rays to be evaluated in parallel
                    __global Mpoint   *mpoints, // For each ray, the set of up to MPSIZE points that are possible matches on a ray
                             float   ppm
                    )
{
  // Rays is the input, mpoints is the output
  // For each ray, perform raytracing until range used up or possible matches filled
  // up. Observed stats suggest that there are usually 2 or 3 hits that are disregarded
  // by the predicate function before reaching a hit which is real. Since we are not running
  // a predicate function, which would require access to the greater datastructure of
  // blocks, rather than just boolean occupancy, we will tune the size of the match array
  int           i   = get_global_id(0);
  __global Ray    *r  = &(rays[i]);
  __global Mpoint *mp = &(mpoints[i]);
  
  float globx = r->origin.x * ppm;
  float globy = r->origin.y * ppm;
  
  // FIXME debugging
  mp->x = globx;
  mp->y = globy;
  mp->a = r->origin.a;
  mp->n = 9999;
  mp->hits = 0;

  // eliminate a potential divide by zero
  const float angle = r->origin.a == 0.0f ? 1e-12f : r->origin.a;
  const float sina = sin(angle);
  const float cosa = cos(angle);
  const float tana = sina/cosa; // approximately tan(angle) but faster
  
  // the x and y components of the ray
  const float dx = ppm * r->range * cosa;
  const float dy = ppm * r->range * sina;
  
  // fast integer line 3d algorithm adapted from Cohen's code from
  // Graphics Gems IV
  const int sx = copysign(1, dx);
  const int sy = copysign(1, dy);
  const int ax = fabs(dx);
  const int ay = fabs(dy);
  const int bx = 2*ax;
  const int by = 2*ay;
  int exy = ay-ax; // difference between x and y distances
  int n   = ax+ay; // the manhattan distance to the goal cell
  
  // the distances between region crossings in X and Y
  const float xjumpx = sx * REGIONWIDTH;
  const float xjumpy = sx * REGIONWIDTH * tana;
  const float yjumpx = sy * REGIONWIDTH / tana;
  const float yjumpy = sy * REGIONWIDTH;
  
  // manhattan distance between region crossings in X and Y
  const float xjumpdist = fabs(xjumpx)+fabs(xjumpy);
  const float yjumpdist = fabs(yjumpx)+fabs(yjumpy);
  
  //const unsigned int layer = (updates+1) % 2;
  
  // these are updated as we go along the ray
  float xcrossx = 0;
  float xcrossy = 0;
  float ycrossx = 0;
  float ycrossy = 0;
  float distX = 0;
  float distY = 0;
  char calculatecrossings = true;

  int iter = 0;
  while((n > 0) && (mp->hits < MPSIZE)) // while we are still not at the ray end
  {
    int sr = GetSuperRegion(world, GETSREG(globx), GETSREG(globy));
    __global char *reg = sr ? GetRegion(world, sr, GETREG(globx), GETREG(globy)) : 0;
    
    if( reg ) // if the region contains any objects
    {
      // invalidate the region crossing points used to jump over
      // empty regions
      calculatecrossings = true;
        
      // convert from global cell to local cell coords
      int cx = GETCELL(globx);
      int cy = GETCELL(globy);

      while( (cx>=0) && (cx<REGIONWIDTH) &&
              (cy>=0) && (cy<REGIONWIDTH) &&
              (n > 0) && (mp->hits < MPSIZE))
        {
          //mp->p[mp->hits].x = world;
          //mp->p[mp->hits++].y = reg;
          // If the cell is occupied, note the coordinate as a match
          if (reg[cx + cy * REGIONWIDTH] != 0)
          {
            mp->p[mp->hits].x = globx;
            mp->p[mp->hits].y = globy;
            mp->hits ++;
          }
          
          // increment our cell in the correct direction
          if( exy < 0 ) // we're iterating along X
          {
            globx += sx; // global coordinate
            exy += by;
            cx += sx; // cell coordinate for bounds checking
          }
          else  // we're iterating along Y
          {
            globy += sy; // global coordinate
            exy -= bx;
            cy += sy; // cell coordinate for bounds checking
          }
          --n; // decrement the manhattan distance remaining
          iter = 1;
        }
      mp->n = n;
    }
    else // jump over the empty region
    {
      // on the first run, and when we've been iterating over
      // cells, we need to calculate the next crossing of a region
      // boundary along each axis
      if( calculatecrossings )
      {
        calculatecrossings = false;
        
        // find the coordinate in cells of the bottom left corner of
        // the current region
        const int ix = globx;
        const int iy = globy;
        float regionx = ix / REGIONWIDTH*REGIONWIDTH;
        float regiony = iy / REGIONWIDTH*REGIONWIDTH;
        if( (globx < 0) && (ix % REGIONWIDTH) ) regionx -= REGIONWIDTH;
        if( (globy < 0) && (iy % REGIONWIDTH) ) regiony -= REGIONWIDTH;
        
        // calculate the distance to the edge of the current region
        const float xdx = sx < 0 ?
                         regionx - globx - 1.0f : // going left
                         regionx + REGIONWIDTH - globx; // going right
        const float xdy = xdx * tana;
        
        const float ydy = sy < 0 ?
                         regiony - globy - 1.0f :  // going down
                         regiony + REGIONWIDTH - globy; // going up
        const float ydx = ydy / tana;
        
        // these stored hit points are updated as we go along
        xcrossx = globx + xdx;
        xcrossy = globy + xdy;
        
        ycrossx = globx + ydx;
        ycrossy = globy + ydy;
        
        // find the distances to the region crossing points
        // manhattan distance is faster than using hypot()
        distX = fabs(xdx) + fabs(xdy);
        distY = fabs(ydx) + fabs(ydy);
      }
        
      if( distX < distY ) // crossing a region boundary left or right
      {
        // move to the X crossing
        globx = xcrossx;
        globy = xcrossy;
        n -= distX; // decrement remaining manhattan distance
        
        // calculate the next region crossing
        xcrossx += xjumpx;
        xcrossy += xjumpy;
        distY -= distX;
        distX = xjumpdist;
      }
      else // crossing a region boundary up or down
      {
        // move to the X crossing
        globx = ycrossx;
        globy = ycrossy;
        n -= distY; // decrement remaining manhattan distance
    
        // calculate the next region crossing
        ycrossx += yjumpx;
        ycrossy += yjumpy;
        distX -= distY;
        distY = yjumpdist;
      }
    }
    mp->n = n;
  }
  // FIXME!! Having this assignment here, rather than at the end of the two blocks
  // within the while loop doesn't work for no reason I can see! Either n should
  // be less than or equal to zero, or mp->hits should be >= MPSIZE. There should be no
  // way to reach here if neither are true, but I am seeing values of n > 0 with
  // hits < 8.. suspect a compiler bug with some sort of temp value within the loop
  // not correctly updating the n outside the loop. Investigating showed that this
  // only occurs with the assigments n -= distXY
  mp->n = n;

}


);

