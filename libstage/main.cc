/**
  \defgroup stage The Stage standalone robot simulator.

  USAGE:  stage [options] <worldfile1> [worldfile2 ... ]

  Available [options] are:

    --clock        : print simulation time peridically on standard output

    -c             : equivalent to --clock

    --gui          : run without a GUI

    -g             : equivalent to --gui

    --help         : print this message

    --args \"str\"   : define an argument string to be passed to all controllers

    -a \"str\"       : equivalent to --args "str"

    -h             : equivalent to --help"

    -?             : equivalent to --help
 */

#include <getopt.h>
#include <sys/resource.h>

#include "stage.hh"
#include "config.h"
using namespace Stg;

const char* USAGE = 
  "USAGE:  stage [options] <worldfile1> [worldfile2 ... worldfileN]\n"
  "Available [options] are:\n"
  "  --clock        : print simulation time peridically on standard output\n"
  "  -c             : equivalent to --clock\n"
  "  --gui          : run without a GUI\n"
  "  -g             : equivalent to --gui\n"
  "  -t <num>       : number of worker threads to use\n"
  "  --help         : print this message\n"
  "  --args \"str\"   : define an argument string to be passed to all controllers\n"
  "  -a \"str\"       : equivalent to --args \"str\"\n"
  "  -h             : equivalent to --help\n"
  "  -?             : equivalent to --help";

/* options descriptor */
static struct option longopts[] = {
	{ "gui",  optional_argument,   NULL,  'g' },
	{ "clock",  optional_argument,   NULL,  'c' },
	{ "threads",  required_argument,   NULL,  't' },
	{ "help",  optional_argument,   NULL,  'h' },
	{ "args",  required_argument,   NULL,  'a' },
	{ NULL, 0, NULL, 0 }
};

int main( int argc, char* argv[] )
{
#ifdef __APPLE__
    struct timeval start_time; gettimeofday(&start_time, NULL);
#else
    struct timespec start_time; clock_gettime(CLOCK_MONOTONIC, &start_time);
#endif
  
  // initialize libstage - call this first
  Stg::Init( &argc, &argv );
  printf("argv[0] is %s\n", argv[0]);


  printf( "%s %s ", PROJECT, VERSION );
  
  int ch=0, optindex=0;
  bool usegui = true;
  bool showclock = false;
  int worker_threads = 1;
  
  while ((ch = getopt_long(argc, argv, "cgh?", longopts, &optindex)) != -1)
	 {
		switch( ch )
		  {
		  case 0: // long option given
			 printf( "option %s given\n", longopts[optindex].name );
			 break;
		  case 'a':
			 World::ctrlargs = std::string(optarg);
			 break;
		  case 'c': 
			 showclock = true;
			 printf( "[Clock enabled]" );
			 break;
		  case 'g': 
			 usegui = false;
			 printf( "[GUI disabled]" );
			 break;
		  case 't': 
			 //worker_threads = strtol(optarg);
			 printf( "[Worker threads %d]", worker_threads );
			 break;
		  case 'h':  
		  case '?':  
			 puts( USAGE );
			 //			 exit(0);
			 break;
		  default:
			 printf("unhandled option %c\n", ch );
			 puts( USAGE );
			 //exit(0);
		  }
	 }
  
  puts("");// end the first start-up line

  
  // arguments at index [optindex] and later are not options, so they
  // must be world file names
  
  optindex = optind; //points to first non-option
  while( optindex < argc )
	 {
		if( optindex > 0 )
		  {      
			 const char* worldfilename = argv[optindex];
			 World* world = ( usegui ? 
										new WorldGui( 400, 300, worldfilename ) : 
									new World( worldfilename ) );
			 world->Load( worldfilename );
			 world->ShowClock( showclock );

			 if( ! world->paused ) 
				world->Start();
		  }
		optindex++;
	 }

  World::Run();  
  
  puts( "\n[Stage: done]" );
#ifdef __APPLE__
  struct timeval end_time; gettimeofday(&end_time, NULL);
  double rtime = ((double)(end_time.tv_sec-start_time.tv_sec))+((double)(end_time.tv_usec-start_time.tv_usec))/1.0e6;
#else
  struct timespec end_time; clock_gettime(CLOCK_MONOTONIC, &end_time);
  double rtime = ((double)(end_time.tv_sec-start_time.tv_sec))+((double)(end_time.tv_nsec-start_time.tv_nsec))/1.0e9;
#endif

    printf("\nReal time, %12.6f, seconds\n", rtime);
    
	return EXIT_SUCCESS;
}
