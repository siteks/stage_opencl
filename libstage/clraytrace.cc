//
//  clraytrace.cc
//  Stage
//
//  Created by Simon Jones on 12/06/2014.
//
//

#include "stage.hh"
#include "region.hh"

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include "clcommon.h"

#include "../assets/raytrace.cl"

using namespace Stg;

CLRayTrace::CLRayTrace()
: raybuf(0),
  matchbuf(0),
  worldbuf(0),
  largest_totalsize(0),
  largest_ray_size(0),
    lstamp(0)
{
  int err = init_cl();
  if (err)
  {
    printf("Error code during CL init was %d\n", err);
    exit(1);
  }
  count = 0;
}

int CLRayTrace::init_cl()
{
  bool  success = true;
  int   errcode;
  
  printf("Initialising OpenCL context, queue, and buffers\n");
#ifdef __APPLE__
  int gpu = 1;
#else
  int gpu = 1;
#endif

  // Get device
  success   &= checkSuccess(clGetDeviceIDs(NULL, gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1, &device_id, NULL));

  // Create a compute context
  context   = clCreateContext(0, 1, &device_id, NULL, NULL, &errcode);
  success   &= checkSuccess(errcode);

  // Create a command queue
  commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &errcode);
  success   &= checkSuccess(errcode);

  
  // Create the compute program from the source buffer
//#ifdef __APPLE__
//  createProgram(context, device_id, "/Users/simonj/new_world/Stage/assets/raytrace.cl", &program);
//#else
//  createProgram(context, device_id, "/home/simonj/new_world/Stage/assets/raytrace.cl", &program);
//#endif

  createProgramFromString(context, device_id, raytracekernel, &program);

  if (!program)
  {
    printf("Error: Failed to create compute program!\n");
    return EXIT_FAILURE;
  }
  
  // Build the program executable
  success &= checkSuccess(clBuildProgram(program, 0, NULL, NULL, NULL, NULL));
  if (!success)
  {
    size_t len;
    char buffer[2048];
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
    printf("%s\n", buffer);
    return EXIT_FAILURE;
  }
  
  // Create the compute kernel in the program we wish to run
  kernel    = clCreateKernel(program, "raytrace", &errcode);
  success   &= checkSuccess(errcode);
  if (!success) exit(1);

  
  printf("Successfully created OpenCL context, command queue, and kernel\n");
  return 0;
}


void CLRayTrace::calc_sizes()
{
  // Traverse the data structure containing the world and parse it into something
  // suitable for OpenCL. We can't use the existing structure because it uses classes
  // and OpenCL is C-based. Also, there is stuff we don't need in the world data
  // structure, all we need is if a cell is occupied.
  
  // SJ just print some stats at first to see if working
  srsize = w->superregions.size();
  int orsize = 0;
  
  for(std::map<point_int_t,SuperRegion*>::iterator it = w->superregions.begin(); it!=w->superregions.end(); it++)
  {
    SuperRegion *sr = (*it).second;
    if (sr->count)
    {
      for(int i = 0; i < SUPERREGIONSIZE; i++)
      {
        Region &r = sr->regions[i];
        if (r.count)
          orsize++;
      }
    }
  }
  
  totalsize = sizeof(int32_t)                             // number of superregions
  + srsize * 2 * sizeof(int32_t)                // superregion origins
  + srsize * SUPERREGIONSIZE * sizeof(int32_t)  // region pointer arrays
  + orsize * REGIONSIZE *  sizeof(char);
  
  /*
   printf("There are %d superregions, and %d occupied regions\n", srsize, orsize);
   printf("Superregion origins: ");
   for(std::map<point_int_t,SuperRegion*>::iterator it = w->superregions.begin(); it!=w->superregions.end(); it++)
   printf("(%d,%d) ",(*it).first.x, (*it).first.y); printf("\n");
   printf("Each superregion contains %d regions\n", SUPERREGIONSIZE);
   printf("Each region contains %d cells\n", REGIONSIZE);
   
   printf("The total size needed is 0x%x bytes\n", totalsize);
   printf("Starting offsets:\n");
   printf("Size                  %08x\n",0);
   printf("SR origins            %08x\n",4);
   printf("SR region pointers    %08x\n",4+srsize*4);
   printf("Occupied region cells %08x\n",4+srsize*(8+SUPERREGIONSIZE*4));
   printf("Cell region should be 0x%lx bytes\n", orsize * REGIONSIZE * sizeof(bool));
   */
}



void CLRayTrace::build_world_structure(char *wb, int layer)
{
  // Structure of data
  //  srsize      4
  //  x0          4
  //  y0          4
  //  x1
  //  y1
  //  ...
  //  xn
  //  yn
  //  regions0    SUPERREGIONSIZE * sizeof(ptr)
  //  regions1
  //  ...
  //  regionsn
  //  cells0      REGIONSIZE * sizeof(bool)
  //  cells1
  //  ...
  //  cellsm
  //
  //  where:
  //  n is srsize,
  //  m is orsize,
  //  regions is SUPERREGIONSIZE * sizeof(ptr)
  //  cells is REGIONSIZE * sizeof(bool)
  
  //NOTE!!------------------------------------------------
  // ALthough the following looks like it is storing pointers in a data structure, they are actually
  // integer byte offsets from the start of the buffer. This is because even though the buffer is
  // mapped, pointers will not be interporeted correctly on the OpenCL side for two reasons
  // a) The memory will be mapped to some other address in the GPU
  // b) The pointer width of Mali is 64 bits, and that of the linux octa is 32 bits, generally
  // it is not possible to rely on pointers in a portable way
  //------------------------------------------------------
  
  // Get a pointer to the start of the cell area
  const int srorigins_size   = 2 * sizeof(int32_t);
  const int srpointers_size  = SUPERREGIONSIZE * sizeof(int32_t);
  
  char *p = wb;
  int32_t c = sizeof(int32_t) + srsize * (srorigins_size + srpointers_size);
  
  // First the superregion size, the number of superregions n
  *(uint32_t*)p = srsize;
  p += sizeof(int32_t);
  
  // Now the x,y origin of each superregion. The map is ordered, with x then y
  // being significant within the point_int_t type. I think this means that the
  // memory ordering is column major, which is not usual for c
  for(std::map<point_int_t,SuperRegion*>::iterator it = w->superregions.begin(); it!=w->superregions.end(); it++)
  {
    *(int32_t*)p = (*it).first.x;
    p += sizeof(int32_t);
    *(int32_t*)p = (*it).first.y;
    p += sizeof(int32_t);
  }
  //uint32_t test_orsize = 0;
  // Now traverse the superregion map again to get the regions and the occupied cells
  //
  // NOTE! There is some subtlety here regarding the two layers. There seems to
  // be the intent to operate a double-buffered structure where updates can happen in
  // one layer while sensors are checked in the other layer. But there aren't really two
  // separate structures: above the cell level, there is only one structure, which is the
  // logical or of the implicit two structures. This means that the position updates
  // operating on the sme structure can mean that things can change underfoot causing
  // the number of occupied regions to change between the first loop above to get orsize,
  // and the loop below to create the cell table. For example, the first pass gets an
  // orsize of 10 and allocates that much memory, then a position update causes an
  // additional region to become occupied.When this happens, the loop below will overrun
  // the allocated memory, with hilarious consequences. I've fixed this temporarily
  // by moving the position updates in world.cc to outside the multithreaded section
  // of Update(), which will probably cause some small drop in performance.
  //
  // This only matters because we are relying on the number of occupied regions remaining
  // unchanged, with the standard update functions, due to the structure being effectively
  // the logical OR of both layers conceptual structures, traversing additional regions
  // will have no bad effects since at the leaf node, the data will still be correctly
  // not present.
  for(std::map<point_int_t,SuperRegion*>::iterator it = w->superregions.begin(); it != w->superregions.end(); it++)
  {
    SuperRegion *sr = (*it).second;
    if (sr->count)
    {
      for(int i=0; i<SUPERREGIONSIZE; i++)
      {
        Region &r = sr->regions[i];
        if (r.count)
        {
          // There is something in this region, so we need to point to
          // a regions worth of cells and fill in the cells
          
          *(int32_t*)p = c;
          char *cptr = (char*)(c+wb);
          unsigned char *cellptr = &(r.bcells[layer][0]);
          memcpy(cptr, cellptr, REGIONSIZE);
          
          c += sizeof(char)*REGIONSIZE;
        }
        else
        {
          // There is nothing in this region, so set the pointer to NULL
          *(int32_t*)p = 0;
        }
        p += sizeof(int32_t);
      }
    }
    else
    {
      p += sizeof(int32_t) * SUPERREGIONSIZE;
    }
  }

}

// This is supposed to be the correct flag for Mali
#ifdef __APPLE__
#define CL_BUFFER_FLAGS 0
#else
#define CL_BUFFER_FLAGS CL_MEM_ALLOC_HOST_PTR
#endif

void CLRayTrace::make_cl_world(int layer)
{

  bool    success = true;
  cl_int  errcode_ret;

  //-----------------------------------------------------------------
  // Find out how big the buffer needs to be to hold the world data structure
  calc_sizes();
  
  // See if there is a buffer and if there is, if it is big enough. If either are
  // not true, we need to create a new buffer. Generally, try and keep using the same
  // buffer unless necessary
  if (totalsize > largest_totalsize || !worldbuf)
  {
    largest_totalsize = totalsize;
    if (worldbuf)
    {
      success &= checkSuccess(clReleaseMemObject(worldbuf));
    }
    worldbuf  = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_BUFFER_FLAGS, totalsize, NULL, &errcode_ret);
    success   &= checkSuccess(errcode_ret);
  }
  
  
  wb        = (char*)clEnqueueMapBuffer(commands, worldbuf, CL_TRUE, CL_MAP_WRITE, 0, totalsize, 0, NULL, NULL, &errcode_ret);
  success   &= checkSuccess(errcode_ret);
  if (!success) exit(1);

  //-----------------------------------------------------------------
  // Traverse the world data structure and create the OpenCL view of it
  build_world_structure(wb, layer);

  //-----------------------------------------------------------------
  // Make the world buffer available to the GPU. For Mali, this means unmapping the buffer. On
  // non-unified architectures, this should mean an implicit copy
  success       &= checkSuccess(clEnqueueUnmapMemObject(commands, worldbuf, wb, 0, NULL, NULL));
  if (!success) exit(1);
  
  /*
  for(int i=0; i<0x11000; i+=4)
  {
    int d = *(int*)(wb+i);
    if (i%32 == 0)
        printf("\n%08x  %08x ", i, d);
    else
        printf("%08x ", d);
  }
  printf("\n");
  */
}

unsigned long stamp() {struct timeval t; gettimeofday(&t,NULL); return t.tv_usec;}

clMpoint *CLRayTrace::run_cl_raytrace(RaySet &rays)
{
  bool success = true;
  cl_int errcode;
  cl_event      event = 0;

    double q,i,r;

  //-----------------------------------------------------------------
  // Allocate the buffer to hold the rays
  int raybufsize    = rays.size() * sizeof(clRay);
  int matchbufsize  = rays.size() * sizeof(clMpoint);
  
  if ((int)rays.size() > largest_ray_size || !raybuf)
  {
    largest_ray_size = rays.size();
    if (raybuf)
    {
      success &= checkSuccess(clReleaseMemObject(raybuf));
      success &= checkSuccess(clReleaseMemObject(matchbuf));
    }
    raybuf        = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_BUFFER_FLAGS, raybufsize, NULL, &errcode);
    success       &= checkSuccess(errcode);
    matchbuf      = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_BUFFER_FLAGS, matchbufsize, NULL, &errcode);
    success       &= checkSuccess(errcode);
  }
  if (!success) exit(1);


  //-----------------------------------------------------------------
  // Map the ray buffer to host
  clRay *rb     = (clRay*)clEnqueueMapBuffer(commands, raybuf, CL_TRUE, CL_MAP_WRITE, 0, raybufsize, 0, NULL, &event, &errcode);
  success       &= checkSuccess(errcode);
  if (!success) exit(1);
  printProfilingInfoReleaseEvent("Map raybuf  ", event);


  //-----------------------------------------------------------------
  // Fill input ray buffer with the rays we wish to trace
  for(unsigned i = 0; i < rays.size(); i++)
  {
    rb[i].origin.x  = rays[i].origin.x;
    rb[i].origin.y  = rays[i].origin.y;
    rb[i].origin.a  = rays[i].origin.a;
    rb[i].range     = rays[i].range;
  }
  
  //STAMP("ray buffer filled");
  //-----------------------------------------------------------------
  // Unmap the world and ray buffers so we can pass it to the kernel
  // (the world buffer was allocated, mapped, and filled in make_cl_world()
  //success       &= checkSuccess(clEnqueueUnmapMemObject(commands, worldbuf, wb, 0, NULL, NULL));
  success       &= checkSuccess(clEnqueueUnmapMemObject(commands, raybuf, rb, 0, NULL, NULL));
  if (!success) exit(1);
  
  //-----------------------------------------------------------------
  // Setup kernel arguements
  success       &= checkSuccess(clSetKernelArg(kernel, 0, sizeof(cl_mem), &worldbuf));
  success       &= checkSuccess(clSetKernelArg(kernel, 1, sizeof(cl_mem), &raybuf));
  success       &= checkSuccess(clSetKernelArg(kernel, 2, sizeof(cl_mem), &matchbuf));
  float ppm = (float)w->ppm;
  success       &= checkSuccess(clSetKernelArg(kernel, 3, sizeof(float), &ppm));
  if (!success) exit(1);
  
  //STAMP("arguments set and ready to NDRange");
  // Start kernel and wait to finish
  size_t        globalWorksize = rays.size();
  success       &= checkSuccess(clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &globalWorksize, NULL, 0, NULL, &event));
  success       &= checkSuccess(clFinish(commands));
  if (!success) exit(1);
  
  // Get and print profiling info, then release the associated event
  unsigned long s = stamp(); lstamp = s - lstamp;
  getProfilingInfoReleaseEvent(event, &q, &i, &r); 
  //printf("Kernel %8u %8lu %8.2f %8.2f %8.2f %4.1f\n", rays.size(), lstamp, q, i, r, r*100.0/lstamp);
  lstamp = s;

  
  //-----------------------------------------------------------------
  // Map output buffer to host
  mb            = (clMpoint*)clEnqueueMapBuffer(commands, matchbuf, CL_TRUE, CL_MAP_READ, 0, matchbufsize, 0, NULL, &event, &errcode);
  success       &= checkSuccess(errcode);
  if (!success) exit(1);
  printProfilingInfoReleaseEvent("Map matchbuf", event);
  //STAMP("finished and buffer remapped");


  /*
  // Show data
  for(unsigned i=0; i<rays.size(); i++)
  {
    printf("Ray %d x:%f y:%f a:%f %d (%d,%d) (%d,%d) (%d,%d) (%d,%d) (%d,%d) (%d,%d) (%d,%d) (%d,%d)\n", i, 
            rays[i].origin.x * w->ppm,
            rays[i].origin.y * w->ppm,
            rays[i].origin.a,
            mb[i].hits,
           mb[i].p[0].x, mb[i].p[0].y,
           mb[i].p[1].x, mb[i].p[1].y,
           mb[i].p[2].x, mb[i].p[2].y,
           mb[i].p[3].x, mb[i].p[3].y,
           mb[i].p[4].x, mb[i].p[4].y,
           mb[i].p[5].x, mb[i].p[5].y,
           mb[i].p[6].x, mb[i].p[6].y,
           mb[i].p[7].x, mb[i].p[7].y
           );
  }*/
  // Return a copy of the pointer to the match data
  return mb;
}


void CLRayTrace::cleanup_cl_objects()
{
  // Unmap buffer
  bool success;
  success       = checkSuccess(clEnqueueUnmapMemObject(commands, matchbuf, mb, 0, NULL, NULL));
  if (!success) exit(1);
  
  // Clean up buffer objects
  //clReleaseMemObject(raybuf);
  //clReleaseMemObject(worldbuf);
  //clReleaseMemObject(matchbuf);

}




