//
//  clraytrace.h
//  Stage
//
//  Created by Simon Jones on 12/06/2014.
//
//

#ifndef __Stage__clraytrace__
#define __Stage__clraytrace__


#include <iostream>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

//#include "stage.hh"
//#include "region.hh"


//-------------------
// C data structures that will contain the OpenCL vision of the world
// These will be mirrored in the OpenCL source code

// FIXME!! SJ This is temp until I work out why I can't
// use the definitions in stage.hh
const int32_t cl_RBITS( 5 );
const int32_t cl_SBITS( 5 );
const int32_t cl_SRBITS( cl_RBITS+cl_SBITS );

const int32_t cl_REGIONWIDTH( 1<<cl_RBITS );
const int32_t cl_REGIONSIZE( cl_REGIONWIDTH*cl_REGIONWIDTH );

const int32_t cl_SUPERREGIONWIDTH( 1<<cl_SBITS );
const int32_t cl_SUPERREGIONSIZE( cl_SUPERREGIONWIDTH*cl_SUPERREGIONWIDTH );

typedef struct {
  int       x;
  int       y;
} clIpoint;

typedef struct {
  float     x;
  float     y;
  float     a;
} clPose;

typedef struct {
  clPose    origin;
  float     range;
} clRay;

// Type to hold matches
#define MPSIZE  16

typedef struct {
  int       hits;
  int       n;
  int       iter;
  float     x;
  float     y;
  float     a;
  clIpoint  p[MPSIZE];
} clMpoint;


namespace Stg
{
  class World;
  
  class Ray;
  
  class Model;
  //class Event;
  
  typedef std::deque<Ray> RaySet;
  //class RaySet
  //{
  //  std::deque<Ray> r;
  //};
  
  class CLRayTrace
  {
    cl_device_id device_id;             // compute device id
    cl_context context;                 // compute context
    cl_command_queue commands;          // compute command queue
    cl_program program;                 // compute program
    cl_kernel kernel;                   // compute kernel
    
    cl_mem raybuf;                      // device memory used for the input array
    cl_mem matchbuf;                    // device memory used for the output array
    cl_mem worldbuf;
    char *wb;
    clMpoint *mb;   // Match buffer
    
    World *w;
    int count;
    
  public:
    CLRayTrace();
    
    void set_world(World *_w) { w = _w; }
    
    void make_cl_world(int layer);
    clMpoint *run_cl_raytrace(RaySet &rays);
    void cleanup_cl_objects();
    
  private:
    int init_cl();
    void calc_sizes();
    void build_world_structure(char *wb, int layer);
    
    int srsize;
    int totalsize;
    int largest_totalsize;
    int largest_ray_size;
    
    unsigned long lstamp;
  };
  
};








#endif /* defined(__Stage__clraytrace__) */
