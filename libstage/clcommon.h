
#ifndef COMMON_H
#define COMMON_H

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include <string>

void printProfilingInfoReleaseEvent(std::string prompt, cl_event event, bool echo=false);
void getProfilingInfoReleaseEvent(cl_event event, double *queuetime, double *issuetime, double *runtime);
bool createProgram(cl_context context, cl_device_id device, std::string filename, cl_program* program);
bool createProgramFromString(cl_context context, cl_device_id device, const char *code, cl_program* program);
std::string errorNumberToString(cl_int errorNumber);
bool checkSuccess(cl_int errorNumber);


#endif
