

#include "clcommon.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>


using namespace std;

void printProfilingInfoReleaseEvent(std::string prompt, cl_event event, bool echo)
{
  bool success            = true;
  cl_ulong queuedTime     = 0;
  cl_ulong submittedTime  = 0;
  cl_ulong startTime      = 0;
  cl_ulong endTime        = 0;
  
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &queuedTime, NULL));
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &submittedTime, NULL));
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &startTime, NULL));
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &endTime, NULL));
  success &= checkSuccess(clReleaseEvent(event));

  if (!success) exit(1);
  
  if (echo)
  printf("%s %8.2f %8.2f %8.2f\n", prompt.c_str(),(submittedTime - queuedTime) / 1000.0, (startTime - submittedTime) / 1000.0, (endTime - startTime) / 1000.0);
}

void getProfilingInfoReleaseEvent(cl_event event, double *queuetime, double *issuetime, double *runtime)
{
  bool success            = true;
  cl_ulong queuedTime     = 0;
  cl_ulong submittedTime  = 0;
  cl_ulong startTime      = 0;
  cl_ulong endTime        = 0;
  
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &queuedTime, NULL));
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &submittedTime, NULL));
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &startTime, NULL));
  success &= checkSuccess(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &endTime, NULL));
  success &= checkSuccess(clReleaseEvent(event));

  if (!success) exit(1);
  
  *queuetime    = (submittedTime - queuedTime) / 1000.0;
  *issuetime    = (startTime - submittedTime) / 1000.0;
  *runtime      = (endTime - startTime) / 1000.0;
}




bool createProgram(cl_context context, cl_device_id device, string filename, cl_program* program)
{
  cl_int errorNumber = 0;
  ifstream kernelFile(filename.c_str(), ios::in);
  
  if(!kernelFile.is_open())
  {
    cerr << "Unable to open " << filename << ". " << __FILE__ << ":"<< __LINE__ << endl;
    return false;
  }
  
  /*
   * Read the kernel file into an output stream.
   * Convert this into a char array for passing to OpenCL.
   */
  ostringstream outputStringStream;
  outputStringStream << kernelFile.rdbuf();
  string srcStdStr = outputStringStream.str();
  const char* charSource = srcStdStr.c_str();
  
  *program = clCreateProgramWithSource(context, 1, &charSource, NULL, &errorNumber);
  if (!checkSuccess(errorNumber) || program == NULL)
  {
    cerr << "Failed to create OpenCL program. " << __FILE__ << ":"<< __LINE__ << endl;
    return false;
  }
  
  /* Try to build the OpenCL program. */
  bool buildSuccess = checkSuccess(clBuildProgram(*program, 0, NULL, NULL, NULL, NULL));
  
  /* Get the size of the build log. */
  size_t logSize = 0;
  clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
  
  /*
   * If the build succeeds with no log, an empty string is returned (logSize = 1),
   * we only want to print the message if it has some content (logSize > 1).
   */
  if (logSize > 1)
  {
    char* log = new char[logSize];
    clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, logSize, log, NULL);
    
    string* stringChars = new string(log, logSize);
    cerr << "Build log:\n " << *stringChars << endl;
    
    delete[] log;
    delete stringChars;
  }
  
  if (!buildSuccess)
  {
    clReleaseProgram(*program);
    cerr << "Failed to build OpenCL program. " << __FILE__ << ":"<< __LINE__ << endl;
    return false;
  }
  
  return true;
}

bool createProgramFromString(cl_context context, cl_device_id device, const char *code, cl_program* program)
{
  cl_int errorNumber = 0;
  
  const char *codearray[] = {code};
  printf("%s\n",code);

  *program = clCreateProgramWithSource(context, 1, codearray, NULL, &errorNumber);
  if (!checkSuccess(errorNumber) || program == NULL)
  {
    cerr << "Failed to create OpenCL program. " << __FILE__ << ":"<< __LINE__ << endl;
    return false;
  }
  
  /* Try to build the OpenCL program. */
  bool buildSuccess = checkSuccess(clBuildProgram(*program, 0, NULL, NULL, NULL, NULL));
  
  /* Get the size of the build log. */
  size_t logSize = 0;
  clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
  
  /*
   * If the build succeeds with no log, an empty string is returned (logSize = 1),
   * we only want to print the message if it has some content (logSize > 1).
   */
  if (logSize > 1)
  {
    char* log = new char[logSize];
    clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, logSize, log, NULL);
    
    string* stringChars = new string(log, logSize);
    cerr << "Build log:\n " << *stringChars << endl;
    
    delete[] log;
    delete stringChars;
  }
  
  if (!buildSuccess)
  {
    clReleaseProgram(*program);
    cerr << "Failed to build OpenCL program. " << __FILE__ << ":"<< __LINE__ << endl;
    return false;
  }
  
  return true;
}

//inline bool checkSuccess(cl_int errorNumber)
bool checkSuccess(cl_int errorNumber)
{
  if (errorNumber != CL_SUCCESS)
  {
    cerr << "OpenCL error: " << errorNumberToString(errorNumber) << endl;
    return false;
  }
  return true;
}

string errorNumberToString(cl_int errorNumber)
{
  switch (errorNumber)
  {
    case CL_SUCCESS:
      return "CL_SUCCESS";
    case CL_DEVICE_NOT_FOUND:
      return "CL_DEVICE_NOT_FOUND";
    case CL_DEVICE_NOT_AVAILABLE:
      return "CL_DEVICE_NOT_AVAILABLE";
    case CL_COMPILER_NOT_AVAILABLE:
      return "CL_COMPILER_NOT_AVAILABLE";
    case CL_MEM_OBJECT_ALLOCATION_FAILURE:
      return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case CL_OUT_OF_RESOURCES:
      return "CL_OUT_OF_RESOURCES";
    case CL_OUT_OF_HOST_MEMORY:
      return "CL_OUT_OF_HOST_MEMORY";
    case CL_PROFILING_INFO_NOT_AVAILABLE:
      return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case CL_MEM_COPY_OVERLAP:
      return "CL_MEM_COPY_OVERLAP";
    case CL_IMAGE_FORMAT_MISMATCH:
      return "CL_IMAGE_FORMAT_MISMATCH";
    case CL_IMAGE_FORMAT_NOT_SUPPORTED:
      return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case CL_BUILD_PROGRAM_FAILURE:
      return "CL_BUILD_PROGRAM_FAILURE";
    case CL_MAP_FAILURE:
      return "CL_MAP_FAILURE";
    case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
      return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case CL_INVALID_VALUE:
      return "CL_INVALID_VALUE";
    case CL_INVALID_DEVICE_TYPE:
      return "CL_INVALID_DEVICE_TYPE";
    case CL_INVALID_PLATFORM:
      return "CL_INVALID_PLATFORM";
    case CL_INVALID_DEVICE:
      return "CL_INVALID_DEVICE";
    case CL_INVALID_CONTEXT:
      return "CL_INVALID_CONTEXT";
    case CL_INVALID_QUEUE_PROPERTIES:
      return "CL_INVALID_QUEUE_PROPERTIES";
    case CL_INVALID_COMMAND_QUEUE:
      return "CL_INVALID_COMMAND_QUEUE";
    case CL_INVALID_HOST_PTR:
      return "CL_INVALID_HOST_PTR";
    case CL_INVALID_MEM_OBJECT:
      return "CL_INVALID_MEM_OBJECT";
    case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
      return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case CL_INVALID_IMAGE_SIZE:
      return "CL_INVALID_IMAGE_SIZE";
    case CL_INVALID_SAMPLER:
      return "CL_INVALID_SAMPLER";
    case CL_INVALID_BINARY:
      return "CL_INVALID_BINARY";
    case CL_INVALID_BUILD_OPTIONS:
      return "CL_INVALID_BUILD_OPTIONS";
    case CL_INVALID_PROGRAM:
      return "CL_INVALID_PROGRAM";
    case CL_INVALID_PROGRAM_EXECUTABLE:
      return "CL_INVALID_PROGRAM_EXECUTABLE";
    case CL_INVALID_KERNEL_NAME:
      return "CL_INVALID_KERNEL_NAME";
    case CL_INVALID_KERNEL_DEFINITION:
      return "CL_INVALID_KERNEL_DEFINITION";
    case CL_INVALID_KERNEL:
      return "CL_INVALID_KERNEL";
    case CL_INVALID_ARG_INDEX:
      return "CL_INVALID_ARG_INDEX";
    case CL_INVALID_ARG_VALUE:
      return "CL_INVALID_ARG_VALUE";
    case CL_INVALID_ARG_SIZE:
      return "CL_INVALID_ARG_SIZE";
    case CL_INVALID_KERNEL_ARGS:
      return "CL_INVALID_KERNEL_ARGS";
    case CL_INVALID_WORK_DIMENSION:
      return "CL_INVALID_WORK_DIMENSION";
    case CL_INVALID_WORK_GROUP_SIZE:
      return "CL_INVALID_WORK_GROUP_SIZE";
    case CL_INVALID_WORK_ITEM_SIZE:
      return "CL_INVALID_WORK_ITEM_SIZE";
    case CL_INVALID_GLOBAL_OFFSET:
      return "CL_INVALID_GLOBAL_OFFSET";
    case CL_INVALID_EVENT_WAIT_LIST:
      return "CL_INVALID_EVENT_WAIT_LIST";
    case CL_INVALID_EVENT:
      return "CL_INVALID_EVENT";
    case CL_INVALID_OPERATION:
      return "CL_INVALID_OPERATION";
    case CL_INVALID_GL_OBJECT:
      return "CL_INVALID_GL_OBJECT";
    case CL_INVALID_BUFFER_SIZE:
      return "CL_INVALID_BUFFER_SIZE";
    case CL_INVALID_MIP_LEVEL:
      return "CL_INVALID_MIP_LEVEL";
    default:
      return "Unknown error";
  }
}


